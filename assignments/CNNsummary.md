## CNN SUMMARY
An image is read in terms of its pixel values. Each pixel has three values called RGB values. Each image is resized and read as a grayscale image

### Why do we need CNN

An image consists a lot of pixels, each pixel has a set of RGB values, with so many values included in each layer the no. of weights increase and in turn increase the computation time. Therefore using **Convolutional Neural Networks** the complexity of structure and computation time can be reduced.

### Building Blocks
1. Convolution
2. ReLU layer
3. Pooling
4. Fully Connected

* Convolution: Different features are selected from an image, these are called filters. These filter matrices are convolved with the images in the training data. The network learns filters that activate when it detects some specific type of feature at some spatial position in the input. The matrix formed after this step then goes under a ReLU function.

* ReLU: Rectified linear unit is a non-saturating linear function _f(x) = max(0,x)_. It effectively removes negative values from an activation map by setting them to zero. Other functions are also used to increase nonlinearity for example hyperbolic function or sigmoid function. 

* Pooling Layer: There are several non-linear functions to implement pooling among which max pooling is the most common. It partitions the input image into a set of non-overlapping rectangles and, for each such sub-region, outputs the maximum.

* Fully Connected: Finally, after several convolutional and max pooling layers, the high-level reasoning in the neural network is done via fully connected layers. Neurons in a fully connected layer have connections to all activations in the previous layer, as seen in regular (non-convolutional) artificial neural networks. 