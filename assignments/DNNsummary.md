### Deep Neural Networks Summary
A simple neural network consists of three layers, input layer, hidden layer and an output layer. A node in a layer is called as a neuron. Different neurons of a single layer are connected to other neurons of a different layer via weights. Initially these weights are randomly given.
![neural network](https://miro.medium.com/max/837/1*AqaRhzBtp6HkN9xmgT_Uow.jpeg)
In modern architectures a Rectified Linear Unit (ReLU) is used as an activation function. Loss functions are used to evaluate the network’s performance during training, the gradient of the change in the loss is back propagated and used to update the weights to minimise the loss. Gradient descent is used to reduce the cost function. 

In the tutorial, DNN was taught using the example of classification of handwritten digits. There were two hidden layers with one input and one output layer. The first hidden layer matched the edges of a digit and the second layer matched the shapes(eg. a line or a loop). By randomly initializing weights an output was derived. The cost function was derived and then the gradient change was backpropogated and proper set of weights were derived. 

Convolutional Neural Networks are used for Image processing

Long short-term memory Network is used for speech Recognition.